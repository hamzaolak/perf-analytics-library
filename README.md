### basic-perf-analytics

A web site performance calculator.

[![npm](https://img.shields.io/npm/v/basic-perf-analytics)](https://www.npmjs.com/package/basic-perf-analytics)
[![GitHub license](https://img.shields.io/github/license/hamzaolak/basic-react-steps)](https://github.com/hamzaolak/basic-react-steps/blob/master/LICENSE)
![npm bundle size](https://img.shields.io/bundlephobia/min/basic-perf-analytics)
![npm bundle size (version)](https://img.shields.io/bundlephobia/minzip/basic-perf-analytics/latest)

### Steps for usage

1. install package

`npm install basic-perf-analytics`

2. Usage

You can call everywhere.If Dom is created, its get the variables from browser.Otherwise If Dom isn't created, its wait for dom to load.

```javascript
import perfAnalytic from 'basic-perf-analytics'
perfAnalytic();
```

3. Watch the performace test

Performance Dashboard: https://jolly-kilby-11041e.netlify.com/

Example Usage: https://github.com/hamzaolak/datocms-Snipcart-Gatsby-Example-demo/blob/master/gatsby-browser.js

Example Demo: https://stoic-ptolemy-29ee6b.netlify.com/

Performance Dashboard: https://jolly-kilby-11041e.netlify.com/
