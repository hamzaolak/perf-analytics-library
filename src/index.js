import { ttfb, fcp, domContentLoaded, windowLoad } from "./eventsTiming";
import {
  scriptTotalDurationTime,
  imageTotalDurationTime,
  cssTotalDurationTime,
  documentTotalDurationTime,
  fontTotalDurationTime
} from "./networkTiming";

let waitingTimeId;

async function perfAnalytic() {
  var oldonload = window.onload;
  const isLoadedPage =
    performance && performance.timing && performance.timing.loadEventEnd;

  if (isLoadedPage) {
    waitingTimeId = setTimeout(() => {
      const postData = generatePostData(performance.timing.loadEventEnd);
      postPerfAnalytics(postData);
    }, 3000);
  } else {
    window.onload = function() {
      var now = new Date().getTime();
      oldonload && oldonload();
      waitingTimeId = setTimeout(() => {
        const postData = generatePostData(now);
        postPerfAnalytics(postData);
      }, 3000);
    };
  }
}

function postPerfAnalytics(postData) {
  fetch(
    "https://cors-anywhere.herokuapp.com/https://perf-analytics.herokuapp.com/api/perfAnalytic",
    {
      method: "POST",
      cache: "no-cache",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify(postData)
    }
  );
  clearTimeout(waitingTimeId);
}
function generatePostData(loadEventEnd) {
  const postData = {
    ttfb: ttfb(),
    fcp: fcp(),
    domContentLoaded: domContentLoaded(),
    windowLoad: windowLoad(loadEventEnd),
    scriptTotalDurationTime: scriptTotalDurationTime(),
    imageTotalDurationTime: imageTotalDurationTime(),
    cssTotalDurationTime: cssTotalDurationTime(),
    documentTotalDurationTime: documentTotalDurationTime(),
    fontTotalDurationTime: fontTotalDurationTime()
  };

  return postData;
}

export default perfAnalytic;
