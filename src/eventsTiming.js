export function ttfb() {
  const timeToFirstByte =
    performance.timing.responseStart - performance.timing.navigationStart;
  return timeToFirstByte;
}

export function fcp() {
  const [firstContentfulPaint] = performance.getEntriesByName(
    "first-contentful-paint"
  );
  return (firstContentfulPaint && firstContentfulPaint.startTime) || 0;
}

export function domContentLoaded() {
  const domLoadTime =
    performance.timing.domContentLoadedEventEnd -
    performance.timing.navigationStart;
  return domLoadTime;
}

export function windowLoad(loadEnd) {
  const windowLoad = loadEnd - performance.timing.navigationStart;
  return windowLoad;
}
