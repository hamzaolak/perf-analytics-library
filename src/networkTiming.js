const initiatorTypes = {
  img: "img",
  xmlhttprequest: "xmlhttprequest"
};

function totalDurationTime(initiatorTypes, docType) {
  const resourceTimings = performance.getEntriesByType("resource");
  let totalDurationTime = 0;
  let minStartTime = Number.MAX_VALUE;
  let maxResponseEnd = 0;
  const endsWithOutUrlParams = value =>
    value && value.split("?")[0].endsWith(`.${docType}`);
    
  resourceTimings.forEach(resouce => {
    if (
      resouce.initiatorType === initiatorTypes ||
      (docType && endsWithOutUrlParams(resouce.name))
    ) {
      if (minStartTime > resouce.startTime && resouce.startTime !== 0) {
        minStartTime = resouce.startTime;
      }
      if (maxResponseEnd < resouce.responseEnd) {
        maxResponseEnd = resouce.responseEnd;
      }
    }
  });
  if (minStartTime === Number.MAX_VALUE || maxResponseEnd === 0) {
    totalDurationTime = 0;
  } else {
    totalDurationTime += maxResponseEnd - minStartTime;
  }
  return totalDurationTime;
}

export function scriptTotalDurationTime() {
  return totalDurationTime(null, "js");
}

export function imageTotalDurationTime() {
  return totalDurationTime(initiatorTypes.img);
}

export function cssTotalDurationTime() {
  return totalDurationTime(null, "css");
}

export function fontTotalDurationTime() {
  return totalDurationTime(null, "woff");
}

export function documentTotalDurationTime() {
  return totalDurationTime(initiatorTypes.xmlhttprequest);
}
